This is the repository for artifacts of Verde and Dist-Verde, the proofs of concepts implementation of interactive runtime verification.

The artifact for our first implementation is in archive verde-artifact.tar.xz. Its documentation is in ARTIFACT_README.md.
The artifact for our second implementation is in archive dist-verde-artifact.tar.xz. Its documentation is in artifact/README.md.
